import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/*  rutas para cual quier conficuracion los quiero a todos  */


/*import { AboutComponent } from './pages/about/about.component';
import { HomesComponent } from './pages/homes/homes.component';
import { ContactComponent } from './pages/contact/contact.component';*/
import { PagesModule } from './pages/pages.module';
import { MenuComponent } from './components/menu/menu.component';
import { HomesComponent } from './pages/homes/homes.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    /*
    AboutComponent,
    HomesComponent,
    ContactComponent,
   */
  HomesComponent,
  AboutComponent,
  ContactComponent,
  MenuComponent
  ],
 exports: [
  HomesComponent,
  AboutComponent,
  ContactComponent
 ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
