import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsComponent } from './posts/Posts.Component';



@NgModule({
  declarations: [PostsComponent],
  imports: [
    CommonModule
  ]
})
export class PagesModule { }
