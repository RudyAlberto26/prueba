import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { HomesComponent } from './pages/homes/homes.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';

const routes: Routes= [

{
 path: 'homes',
 component: HomesComponent
},
{
  path: 'about',
  component: AboutComponent
},
{
 path: 'contact',
 component: ContactComponent
}
,
{
 path: 'posts',
 loadChildren: './pages/posts/posts.module#PostsModule'
}
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[
  RouterModule
  ]
})
export class AppRoutingModule { }
