import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

routas = [
{
  name: 'home',
  path: '/homes'
},

{
  name: 'about',
  path: '/about'
},

{
  name: 'contact',
  path: '/contact'
},
{
name: 'posts',
path: '/posts'
},


];
  constructor() {}

  ngOnInit() {
  }
}
